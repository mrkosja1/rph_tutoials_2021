class MyMatrix:
    def __init__(self, matice=[]):
        self.matice = matice

    def save(self, filename):
        string_to_save = ""
        for row in self.matice:
            for value in row:
                # string_to_save += f"{char} " # f-string
                string_to_save += str(value) + " "
            string_to_save += "\n"

        with open(filename, 'wt', encoding='utf-8') as f:
            f.write(string_to_save)

    def load(self, filename):
        with open(filename, 'rt', encoding='utf-8') as f:
            string_rows = f.readlines()

        for str_row in string_rows:
            list_of_string_vals = str_row.strip().split(' ')
            row = [int(v) for v in list_of_string_vals]
            self.matice.append(row)


    def get_matrix(self):
        return self.matice


if __name__ == '__main__':
    a = MyMatrix([[1, 2, 3], [2, 3, 4]])
    a.save('matrix.txt')
    b = MyMatrix()
    b.load('matrix.txt')

    assert a.get_matrix() == b.get_matrix()

