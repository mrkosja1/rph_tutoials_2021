# r-read, w-write, a-append
# t-text, b-binary

with open('soubor.txt', 'at', encoding="utf-8") as f:
    f.write("\nMoje dalsi zprava!")
    f.writelines(['ahoj', 'psse', '\n', 'kocka'])

with open('soubor.txt', 'rt', encoding="utf-8") as f:
    # zprava = f.read(5)
    # lines = f.readlines()
    line1 = f.readline()
    line2 = f.readline()
    line3 = f.readline()


# print(zprava)
# print(lines)
print(line1)
print(line2)
