d = {0: "auto", 1: (1, 2), "hj4": 58, ("a", 1): 3}
d_empty = {}  # dict()
print(d)
print(d["hj4"])
d_empty["strejda"] = ("jirka", "anna")
print(d_empty)
# d[89]
print(d.get(89, "jednicka"))
print(list(d.keys()))
print(list(d.values()))
print(list(d.items()))

